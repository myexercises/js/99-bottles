import {capitalize, downTo} from './helpers';
import BottleNumber from './bottleNumber';

export default class Bottles {
	verse(number) {
		const bottleNumber = BottleNumber.for(number);
		return (
			`${capitalize(bottleNumber.toString())} of beer on the wall, ` +
					`${bottleNumber.toString()} of beer.\n` +
					`${bottleNumber.action()} ` +
					`${bottleNumber.successor().toString()} of beer on the wall.\n`
		);
	}

	verses(starting, ending) {
		return (
			downTo(starting, ending)
				.map(number => new Bottles().verse(number))
				.join('\n')
		);
	}

	song() {
		return this.verses(99, 0);
	}
}
