import BottleNumber from './bottleNumber';

export default class BottleNumber0 extends BottleNumber {
	quantity() {
		return 'no more';
	}

	action() {
		return 'Go to the store and buy some more,';
	}

	successor() {
		return 99;
	}
}
