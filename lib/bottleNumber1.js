import BottleNumber from './bottleNumber';

export default class BottleNumber1 extends BottleNumber {
	container() {
		return 'bottle';
	}

	pronoun() {
		return 'it';
	}
}
