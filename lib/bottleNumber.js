export default class BottleNumber {
	constructor(number) {
		this.number = number;
	}

	static for(number) {
		let bottleNumberClass;
		switch (number) {
		case 0:
			bottleNumberClass = BottleNumber0;
			break;
		case 1:
			bottleNumberClass = BottleNumber1;
			break;
		case 6:
			bottleNumberClass = BottleNumber6;
			break;
		default:
			bottleNumberClass = BottleNumber;
		}

		return new bottleNumberClass(number);
	}

	container() {
		return 'bottles';
	}

	pronoun() {
		return 'one';
	}

	quantity() {
		return this.number.toString();
	}

	action() {
		return `Take ${this.pronoun()} down and pass it around,`;
	}

	successor() {
		return BottleNumber.for(this.number - 1);
	}

	toString() {
		return `${this.quantity()} ${this.container()}`;
	}
}

class BottleNumber6 extends BottleNumber {
	container() {
		return 'six-pack';
	}

	quantity() {
		return '1';
	}

}
class BottleNumber1 extends BottleNumber {
	container() {
		return 'bottle';
	}

	pronoun() {
		return 'it';
	}
}

class BottleNumber0 extends BottleNumber {
	quantity() {
		return 'no more';
	}

	action() {
		return 'Go to the store and buy some more,';
	}

	successor() {
		return BottleNumber.for(99);
	}
}
